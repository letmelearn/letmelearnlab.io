export const arrayToRgbStr = (color: Array<number>, alpha?: number) => {
  let colorStyle = "rgba(";
  if(color.length === 3) {
    if(alpha && alpha < 1 && alpha > 0){
      for(let i = 0; i < 3; i++) colorStyle += `${color[i]},`;
      colorStyle += `${alpha})`
    }
    else {
      for(let i = 0; i < 2; i++) colorStyle += `${color[i]},`
      colorStyle += `${color[2]})`
    }
  }
  else if(color.length == 4) {
      for(let i = 0; i < 3; i++) colorStyle += `${color[i]},`
      colorStyle += `${color[3]})`
  }
  else throw new Error("Invalid Color Array");
  return colorStyle;
}
