export const getImage = (name: string, extension: string = "jpg") => {
  const img = require(`Images/${name}.${extension}`);
  return img.default;
}
