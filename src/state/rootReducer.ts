import { combineReducers } from "redux";
import subjectReducer from "./subjectReducer.ts";

export default combineReducers({
  subject: subjectReducer
});
