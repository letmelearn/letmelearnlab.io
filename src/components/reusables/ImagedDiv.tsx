import React from "react";
import { getRdmColor } from "@utilities/randomUtils.ts";
import { arrayToRgbStr } from "@utilities/cssUtils.ts";

interface Props {
  image: any
  children: any
  colored: boolean
}

const ImagedDiv = (props: Props) => {
  const { image, children, colored } = props;
  const style: React.CSSProperties = {};

  if(colored) style.backgroundColor = arrayToRgbStr(getRdmColor(), 0.4);

  return (
    <div className="background">
      <img src={image} className="coloredBackgroundImage"/>
      <div className="colorLayer" style={style}>
          {children}
      </div>
    </div>
  )

}

export default ImagedDiv;


