
import React from "react";
import { connect } from "react-redux";
import typewriter from "Images/typewriter.jpg"

interface Props {
  children: any
}

const CenteredText = (props: Props) => {
  const { children } = props;
  return (
    <div className="centered">
      {children}
    </div>
  )
}

export default CenteredText;

