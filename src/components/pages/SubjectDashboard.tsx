import React from "react";
import { connect } from "react-redux";
import ResourceBar from "@components/pages/pageComponents/subjectDashboard/ResourceBar"
import "Styles/dashboard.css";
import SubjectHeader from "@components/pages/pageComponents/subjectDashboard/SubjectHeader";
import ResourceList from "@components/pages/pageComponents/subjectDashboard/ResourceList";

interface Props {

}

interface Handlers {

}

const SubjectDashboard = (props: Props & Handlers) => {
  return (
    <div className="subjectDashboard">
      <SubjectHeader />
      <ResourceBar />
      <ResourceList />

    </div>
  )
}

const mapStateToProps = (state: any) => {
  return {

  }
}

const mapDispachToProps = (dispatch: Function) => {
  return {

  }
}

export default connect(
  mapStateToProps,
  mapDispachToProps
)(SubjectDashboard);
