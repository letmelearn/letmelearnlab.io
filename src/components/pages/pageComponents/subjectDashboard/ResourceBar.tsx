import React from "react";
import { connect } from "react-redux";
import "Styles/dashboard.css";
import ActionImage from "@components/reusables/ActionImage";

interface Props {

}

interface Handlers {

}

const ResourceBar = (props: Props & Handlers) => {
  return (
    <div className="subjectResourceBar">
      <ActionImage
        imageUrl="greenCircle.png"
        className=""
        height={50}
        width={50}
        action={(event: any) => console.log("test")}
      />
      <ActionImage
        imageUrl="greenCircle.png"
        className=""
        height={50}
        width={50}
        action={(event: any) => console.log("test2")}
      />
    </div>
  )
}

const mapStateToProps = (state: any) => {
  return {

  }
}

const mapDispachToProps = (dispatch: Function) => {
  return {

  }
}

export default connect(
  mapStateToProps,
  mapDispachToProps
)(ResourceBar);
