import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import Landing from "@components/pages/Landing";
import Dashboard from "@components/pages/SubjectDashboard";

export default () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Landing />
        </Route>
        <Route path="/dashboard" >
          <Dashboard />
        </Route>
        <Route path="/signin" >
          <Landing />
        </Route>
      </Switch>
    </Router>
  )
}
