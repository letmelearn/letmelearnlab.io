const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const webpack = require("webpack");

node: {
	fs: 'empty'
}

module.exports = {
	devtool: 'source-map',
	entry: "./src/index.tsx",
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [
					'file-loader'
				]
			},
			{
				test: /\.css?$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							publicPath: '/src/styles/'
						}
					},
					'css-loader'
				],
			},
		],
	},
	resolve: {
		alias: {
			Images: path.resolve(__dirname, 'publicAssets/images'),
			Utilities: path.resolve(__dirname, 'src/utilities'),
			Components: path.resolve(__dirname, 'src/components'),
			Styles: path.resolve(__dirname, 'src/styles'),
		},
		extensions: [".tsx", ".tx", ".js"],
		plugins: [
			new TsconfigPathsPlugin()
		]
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: path.join(__dirname, "/publicAssets/index.html")
		}),
		new MiniCssExtractPlugin({
		})
	],
	devServer: {
		historyApiFallback: true
	},
	output: {
		path: path.join(__dirname, "/public"),
		filename: "bundle.js"
	},
}
